;;; -*- lexical-binding:t ; -*-

(unless (bound-and-true-p package--initialized) ; To avoid warnings in 27
  (setq package-enable-at-startup nil))

;;* Site-lisp
(defvar cm/site-lisp-directory (expand-file-name
                                "site-lisp"
                                cm/library-files-directory)
  "The packages.")

(defvar cm/autoloads-file (expand-file-name
                           "loaddefs.el" cm/config-files-directory)
  "Autoloads definition file.")

;; Load path
(let ((default-directory (file-name-as-directory cm/site-lisp-directory)))
  (push default-directory load-path)
  (normal-top-level-add-subdirs-to-load-path))


(defun cm/generate-autoloads ()
  "Generate autoload files recursively for all package in DIR to file TARGET."
  (interactive)
  (require 'al-collector)
  (al-collector-generate-autoloads! cm/site-lisp-directory
                                    cm/autoloads-file)
  (load cm/autoloads-file 'noerror 'no-message))

(if (file-exists-p cm/autoloads-file)
    (load cm/autoloads-file :no-error :no-message)
  (cm/generate-autoloads))

;;* Use-pacakge
(setq use-package-always-defer t
      use-package-always-ensure nil
      use-package-expand-minimally t)

;; Fire up `use-package'
(require 'use-package)

(use-package use-package
  :config
  (add-to-list 'use-package-deferring-keywords :after-call nil #'eq)

  (setq use-package-keywords
        (use-package-list-insert :after-call use-package-keywords :after))
  (add-to-list 'use-package-keywords :homepage)
  (add-to-list 'use-package-keywords :doc)

  (defun cm/use-package-ignore-current-keyword (name _keyword _args rest state)
    "Simply ignore current keywords and all args it has, then continue processing."
    (use-package-process-keywords name rest state))

  (defalias 'use-package-normalize/:doc 'ignore)
  (defalias 'use-package-handler/:doc 'cm/use-package-ignore-current-keyword)

  (defalias 'use-package-normalize/:homepage 'ignore)
  (defalias 'use-package-handler/:homepage 'cm/use-package-ignore-current-keyword)

  (defalias 'use-package-normalize/:after-call 'use-package-normalize-symlist)

  (defun use-package-handler/:after-call (name _keyword hooks rest state)
    (if (plist-get state :demand)
        (use-package-process-keywords name rest state)
      `((dolist (hook ',hooks)
          (cm/add-temp-hook hook
              (require ',name)))
        ,@(use-package-process-keywords name rest state)))))

(use-package async
  :doc "Asynchronous processing"
  :homepage "https://github.com/jwiegley/emacs-async"
  :init
  ;; Fix dired-async-mode with sudo buffers
  ;; https://github.com/jwiegley/emacs-async/issues/91
  (setq async-quiet-switch "-q")
  (async-bytecomp-package-mode)
  (dired-async-mode))

(use-package borg
  :doc "Manage package as git submodules"
  :homepage "https://github.com/emacscollective/borg"
  :init
  (setq borg-drone-directory cm/site-lisp-directory
        borg-user-emacs-directory user-emacs-directory
        borg-gitmodules-file (expand-file-name ".gitmodules"
                                               user-emacs-directory)))

(use-package no-littering
  :doc "Keep .emacs.d clean"
  :demand t
  :init
  (setq no-littering-etc-directory
        cm/cache-files-directory)
  (setq no-littering-var-directory
        cm/cache-files-directory))

;; A useful macro for configuration
(defalias 'cm/add-temp-hook #'walle-package-add-temp-hook)

(put 'cm/add-temp-hook 'lisp-indent-function 1)

(put 'define-package 'doc-string-elt 3)

(put 'gv-define-setter 'doc-string-elt 3)

(provide 'init-package)
;;; init-package.el ends here
