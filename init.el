;;; -*- lexical-binding:t ; -*-
;; (setq debug-on-error t)


;;Without this comment package.el add `package-initialize' here
;; (package-initialize)

(eval-when-compile
  (let ((minver "26.1"))
    (when (version< emacs-version minver)
      (error "Your Emacs don't support this config, use Emacs %s or above" minver))))

;;* Constants Setup

(defvar cm/config-files-directory (expand-file-name "etc" user-emacs-directory)
  "The directory to store configuration files.")

(defvar cm/cache-files-directory (expand-file-name "var" user-emacs-directory)
  "The directory to store the dotfiles create by different extensions.")

(defvar cm/library-files-directory (expand-file-name "lib" user-emacs-directory)
  "The directory to store extensions files, whether from ELPA or Github.")

(defvar cm/third-party-files-directory (expand-file-name "opt" user-emacs-directory)
  "The directory to store third party binary tools.")

(unless (file-directory-p cm/cache-files-directory)
  (mkdir cm/cache-files-directory))

(setq custom-file (expand-file-name "custom.el" cm/config-files-directory))

(when (file-exists-p custom-file) (load custom-file :no-error :no-message))

(defvar cm/gc-cons-threshold-up-limit (* 100 1024 1024))

(defvar cm/gc-cons-threshold-default (* 20 1024 1024))

(defun cm/inc-gc-cons-threshold! ()
  "Increase `gc-cons-threshold' to `cm/gc-cons-threshold-up-limit'."
  (setq gc-cons-threshold cm/gc-cons-threshold-up-limit))

(defun cm/reset-gc-cons-threshold! ()
  "Rest `gc-cons-threshold' to `cm/gc-cons-threshold-default'."
  (setq gc-cons-threshold cm/gc-cons-threshold-default))

;;* Avoid Emacs do GC during the initializing
(let ((default-file-name-handler-alist file-name-handler-alist))
  (cm/inc-gc-cons-threshold!)
  (setq file-name-handler-alist nil)
  (add-hook 'emacs-startup-hook
            (lambda ()
              (setq file-name-handler-alist
                    default-file-name-handler-alist)
              (cm/reset-gc-cons-threshold!)
              (add-hook 'minibuffer-setup-hook
                        #'cm/inc-gc-cons-threshold!)
              (add-hook 'minibuffer-exit-hook
                        #'cm/reset-gc-cons-threshold!)
              (add-hook 'focus-out-hook #'garbage-collect))))


;;* Come on, Let's go!
(defsubst cm/require (file)
  "Load my config files"
  (load (expand-file-name (symbol-name file)
                          cm/config-files-directory)
        :no-error :no-message))

;; Frequent using built-in packages
(require 'cl-lib)
(require 'subr-x)

;; Package management
(cm/require 'init-package)

;; UI
(cm/require 'init-ui)

;; Basic
(cm/require 'init-basic)
;; (cm/require 'init-ivy)
(cm/require 'init-helm)

;; Better editing
(cm/require 'init-edit)
(cm/require 'init-prog-edit)

;; Completions (YAS, Company and others)
(cm/require 'init-completion)

;; ;; Markup-language
;; (cm/require 'init-plantuml)
(cm/require 'init-org)

;; ;; Programming Language
(cm/require 'init-cc)
(cm/require 'init-jts)
;; (cm/require 'init-racket)
(cm/require 'init-emacs-lisp)
(cm/require 'init-common-lisp)

;; ;; Chinese language support
(cm/require 'init-chinese)

;; ;; File-managemnet
(cm/require 'init-dired)
(cm/require 'init-project)
(cm/require 'init-vc)

;; ;; Applications
(cm/require 'init-shell)
(cm/require 'init-wl)
(cm/require 'init-net)
;; (cm/require 'init-emms)

(provide 'init)

;; Local Variables:
;; no-byte-compile: t
;; End:
;;; init.el ends here
