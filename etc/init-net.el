;;; -*- lexical-binding: t; -*-

(use-package telega
  :init
  (setq telega-proxies '((:server "localhost" :port 10808
                          :enable t :type (:@type "proxyTypeSocks5"))))
  (setq telega-completing-read-function 'helm--completing-read-default)
  (setq telega-chat-fill-column 65)
  (setq telega-emoji-use-images nil)
  :config
  (with-eval-after-load 'company
    (add-hook 'telega-chat-mode-hook (lambda ()
                                       (make-local-variable 'company-backends)
                                       (dolist (it '(telega-company-botcmd
                                                     telega-company-emoji))
                                         (push it company-backends)))))
  (telega-notifications-mode))

(use-package helm-telega
 :init
   (setq helm-telega-stickerset-preview-count 1)
 :after telega
 :bind
   (:map telega-chat-mode-map
         ("C-c C-s" . helm-telega-sticker-mini)
         ("C-c C-v" . helm-telega-stickerset-choose)))

(use-package url
 :init
   (setq url-gateway-method 'socks))

(use-package socks
 :init
   (setq socks-noproxy '("127.0.0.1"
                         "localhost"))
   (setq socks-server '("Default Server" "localhost" 10808 5)))

(provide 'init-net)
;;; init-net.el ends here
