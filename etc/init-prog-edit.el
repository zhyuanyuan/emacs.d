;;; -*- lexical-binding:t ; -*-

;;* Tab and space
;; Permanently indent with spaces, never with TABs
(setq-default c-basic-offset   2
              tab-width        8
              indent-tabs-mode nil)

;;* Endless Parenthese
;; Showing parens
;; Silence noisy parentheses
(use-package paren-face
  :hook (prog-mode . paren-face-mode)
  :init
  ;; All "[({" will be silenced.
  (setq paren-face-regexp "[][(){}]"))

;; (defun advice-show-paren-function (fn)
;;   (cond ((looking-at-p "(\\|)") (funcall fn))
;;         (t (save-excursion
;;              (ignore-errors (backward-up-list))
;;              (funcall fn)))))

;; (advice-add #'show-paren-function :around #'advice-show-paren-function)

;; Editing parens
(use-package lispy
  :doc "Sweet and fast lispy editing"
  :homepage "https://github.com/abo-abo/lispy"
  :hook ((lisp-mode
          racket-mode
          emacs-lisp-mode
          scheme-mode) . lispy-mode)
  :init
  (defvar lispy-close-quotes-at-end-p)
  (setq lispy-close-quotes-at-end-p t)
  (setq lispy-completion-method 'helm)
  ;; I don't found `iedit' useful
  (provide 'iedit)
  :config
  (defalias 'lispy-iedit 'ignore)
  (lispy-set-key-theme '(special lispy))
  (add-hook 'lispy-mode-hook #'turn-off-smartparens-mode))

;;* Line numbering
(use-package display-line-numbers
  :ensure nil
  :hook (prog-mode . display-line-numbers-mode))

;;* Quick and general commenter
(use-package evil-nerd-commenter
  :doc "Quick and general commenter."
  :homepage "https://github.com/redguardtoo/evil-nerd-commenter"
  :bind
  ("M-;" . evilnc-comment-or-uncomment-lines))

;;; Indentation
(use-package aggressive-indent
  :doc "Force your code obey the indentation rule."
  :homepage "https://github.com/Malabarba/aggressive-indent-mode"
  :hook
  ;; Some modes conflicts with `aggressive-indent'
  ((emacs-lisp-mode
    ielm-mode
    lisp-mode) . aggressive-indent-mode)
  ;; FIXME: Disable in big file due to the performance issues
  ;; https://github.com/Malabarba/aggressive-indent-mode/issues/73
  (find-file . (lambda ()
                 (when (> (buffer-size) (* 3000 80))
                   (aggressive-indent-mode -1))))
  :config
  ;; Disable in some mode
  (dolist (mode '(html-mode web-mode css-mode))
    (push mode aggressive-indent-excluded-modes)))

(use-package electric
  :preface
  (defvar-local cm/electric-indent-words '())
  :hook ((prog-mode conf-mode) . electric-indent-mode)
  :config
  (defun cm/electric-indent-word-fn (_)
    (when (and (eolp) cm/electric-indent-words)
      (save-excursion
        (backward-word)
        (looking-at-p (concat "\\<" (regexp-opt cm/electric-indent-words))))))

  (add-to-list 'electric-indent-functions #'cm/electric-indent-word-fn))

;;* Syntax checking
(use-package flycheck
  :hook (prog-mode . flycheck-mode)
  :init
  (setq flycheck-emacs-lisp-load-path 'inherit)
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

;;* LSP
(use-package lsp-mode
  :commands lsp
  :hook
  ((js-mode
    js2-mode
    typescript-mode
    sh-mode
    python-mode
    ;; RLS is broken since I update to 1.38
    rust-mode
    ) . lsp)
  (java-mode . (lambda ()
                 (require 'lsp-java)
                 (lsp)))
  ((c-mode c++-mode) . (lambda ()
                         (require 'ccls)
                         (lsp)))
  :init
  ;; Auto suggest root
  (setq lsp-auto-guess-root t)
  ;; Configure it manually.
  (setq lsp-auto-configure nil)
  ;; RLS always crash :)
  (setq lsp-restart 'auto-restart)

  ;; Use Flycheck
  (setq lsp-prefer-flymake nil)
  :config
  ;; Load `lsp-clients'
  (require 'lsp-clients)

  (use-package lsp-ui
    :init
    (setq lsp-ui-sideline-show-code-actions nil
          lsp-ui-sideline-show-hover nil)

    :hook (lsp-mode . lsp-ui-mode)
    :config
    (use-package lsp-ui-peek
      :bind
      (:map lsp-ui-mode-map
            ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
            ([remap xref-find-references] . lsp-ui-peek-find-references))))

  (use-package company-lsp
    :after company
    :defines company-backends
    :functions cm/company-backend-with-yas
    :init
    (setq company-lsp-cache-candidates 'auto)
    (push (if (fboundp 'company-tabnine)
              '(company-lsp :with company-yasnippet company-tabnine)
            #'company-lsp) company-backends)))

(with-eval-after-load 'lsp-mode
  (push '(company-lsp :with company-yasnippet) company-backends))

(provide 'init-prog-edit)
;;; init-prog-edit.el ends here
