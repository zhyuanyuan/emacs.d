;;; -*- lexical-binding: t; -*-

(require 'walle-ui)

;;* Set up the themes
(use-package doom-themes
  :doc "Beautiful themes."
  :demand t
  :config
  (load-theme (if (display-graphic-p)
                  'doom-city-lights
                'doom-one)))

(use-package doom-modeline
  :doc "Beautiful modeline"
  :defer 1
  :init
  (setq doom-modeline-buffer-file-name-style 'buffer-name)
  :config
  (doom-modeline-mode))

(when (walle-ui-display-color-emoji?)
  (set-fontset-font t 'symbol (font-spec :family "Noto Color Emoji")
                    nil 'prepend))

(defun cm/pop-up-this-frame ()
  (x-focus-frame (selected-frame)))

(provide 'init-gui)
;;; init-gui.el ends here
