;;; -*- lexical-binding:t ; -*-

;;* Personal info
(setq user-full-name "Zhu Zihao"
      user-mail-address "all_but_last@163.com")

;;* `*scratch*' buffer
(setq-default initial-scratch-message ""
              default-directory (expand-file-name "~/"))

;; Emacs in Linux is much faster than in Windows.
(setq initial-major-mode #'lisp-interaction-mode)

;; (setq initial-major-mode #'text-mode)

;;* Save-history
(use-package savehist
  :after-call post-command-hook
  :init
  (setq enable-recursive-minibuffers t  ; Allow commands in minibuffers
        history-length 1000
        savehist-additional-variables '(mark-ring
                                        global-mark-ring
                                        search-ring
                                        regexp-search-ring
                                        extended-command-history)
        savehist-autosave-interval 60)
  :config
  (savehist-mode 1))

;;* Recent files
(use-package recentf
  :after-call after-find-file
  :ensure nil
  :init
  (setq recentf-max-saved-items 200)
  :config
  (recentf-mode)
  (recentf-track-opened-file))

;;* Helpful
(use-package helpful
  :doc "A more detailed `*Help*' system"
  :bind
  ("C-c C-d" . helpful-at-point)
  (:map help-map
        ("k" . helpful-key)
        ("F" . helpful-command)
        ("f" . helpful-callable)
        ("v" . helpful-variable)))

(use-package woman
  :bind
  ("C-S-h" . woman))

(use-package server
  :unless (daemonp)
  :hook (after-init . (lambda ()
                        (require 'server)
                        (unless (server-running-p)
                          (server-mode))))
  :init
  (setq server-auth-dir (expand-file-name "server/" cm/cache-files-directory)))

(use-package ace-window
  :doc "Switch between windows."
  :commands (ace-window-display-mode ace-window)
  :custom-face
  (aw-leading-char-face ((t (:inherit 'font-lock-keyword-face :height 2.0))))
  (aw-mode-line-face    ((t (:inherit 'mode-line-emphasis :bold t))))
  :bind
  ("C-x o" . ace-window)
  :config
  ;; Inherits settings from `avy'
  (setq aw-keys avy-keys)
  (setq aw-background avy-background)
  (ace-window-display-mode))

(use-package winner
  :commands (winner-undo)
  :hook (after-init . winner-mode)
  :init
  (setq winner-boring-buffers '("*Completions*"
                                "*Compile-Log*"
                                "*inferior-lisp*"
                                "*Fuzzy Completions*"
                                "*Apropos*"
                                "*Help*"
                                "*cvs*"
                                "*Buffer List*"
                                "*Ibuffer*"
                                "*esh command on file*"))
  :bind
  ("C-z" . winner-undo))

(use-package midnight
  :doc "Clean buffers at midnight."
  :defer 5
  :init
  (setq clean-buffer-list-delay-general 1
        clean-buffer-list-kill-regexps '("\\`\\*\\(?:Wo\\)?Man "
                                         "\\`magit\\(?:-.+\\)?: "
                                         "\\`\\*helpful ")
        clean-buffer-list-kill-buffer-names '("*Help*"
                                              "*Apropos*"
                                              "*Buffer List*"
                                              "*Compile-Log*"
                                              "*info*"
                                              "*vc*"
                                              "*vc-diff*"
                                              "*diff*"
                                              "*Pp Eval Output*"
                                              "*WoMan-Log*"))
  :config
  (midnight-mode 1))

(use-package time
  :doc "Show Time at modeline."
  :init
  (setq display-time-format "%02Y-%02m-%02d %H:%M")
  :hook (after-init . display-time-mode))

;;* Miscs
(advice-add #'yes-or-no-p :override #'y-or-n-p)

;; Enable some disabled functions
(put 'list-timers 'disabled nil)
(put 'erase-buffer 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(defun cm/find-emacs-init-file ()
  "Find my emacs init file"
  (interactive)
  (find-file (expand-file-name "init.el" user-emacs-directory)))

(defun cm/jump-emacs-etc-directory ()
  "Jump to my emacs etc directory"
  (interactive)
  (dired cm/config-files-directory))

(defun cm/jump-emacs-lib-directory ()
  "Jump to my emacs lib directory"
  (interactive)
  (dired cm/library-files-directory))

;;* Keybindings

(use-package bind-key
  :doc "Short-key manager"
  :homepage "https://github.com/jwiegley/use-package")

(use-package hydra
  :doc "Make your key-bindings stick around"
  :homepage "https://github.com/abo-abo/hydra")


(bind-keys ("C-0" . walle-basic-alternate-buffer)
           ("C-<backspace>" . delete-window)
           ("M-0" . other-window)
           ("C-|" . split-window-right)
           ("C--" . split-window-below)
           ("C-c e e" . cm/jump-emacs-etc-directory)
           ("C-c e l" . cm/jump-emacs-lib-directory)
           ("C-c e i" . cm/find-emacs-init-file)
           ("C-c b s" . walle-basic-switch-to-scratch-buffer)
           ("C-c b m" . walle-basic-switch-to-message-buffer)
           ("C-x M-f" . walle-files-sudo-find-file)
           ("C-x M-t" . walle-files-sudo-this-file)
           ("C-!" . kill-current-buffer))

(provide 'init-basic)
;;; init-basic.el ends here
