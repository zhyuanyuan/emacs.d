;;; -*- lexical-binding:t ; -*-

(use-package ivy
  :doc "General minibuffer-completion framework"
  :homepage "https://github.com/abo-abo/swiper"
  :commands ivy-mode
  :defer 1
  :after-call pre-command-hook
  :init
  (setq enable-recursive-minibuffers t)
  (setq ivy-use-selectable-prompt t
        ivy-use-virtual-buffers t
        ivy-initial-inputs-alist nil
        ivy-format-functions-alist '((t . ivy-format-function-arrow))
        ivy-on-del-error-function nil
        ivy-height 17
        ivy-fixed-height-minibuffer t
        ivy-count-format "(%d/%d) "
        ivy-on-del-error-function nil
        ivy-dynamic-exhibit-delay-ms 200)
  :bind
  ("C-c C-r" . ivy-resume)
  :config
  (ivy-mode))

(use-package ivy-hydra
  :doc "A hydra for better `ivy' experience")

(use-package swiper
  :doc "Enchanced I-search with fancy UI"
  :bind
  ("C-s" . swiper)
  (:map swiper-map
        ("M-q" . swiper-query-replace))
  :init
  (setq swiper-action-recenter t))

(use-package counsel
  :doc "A pack of useful functions based-on `ivy'"
  ;; Start follow `ivy-mode'
  :hook (ivy-mode . counsel-mode)
  :bind
  (:map counsel-mode-map
        ("C-c r" . counsel-rg)
        ("C-x C-r" . counsel-recentf)
        ("C-c M-l" . counsel-locate))
  :config
  ;; Use faster search tools: ripgrep or the silver search
  (let ((command
         (cond
           ((executable-find "rg")
            "rg -i -M 120 --no-heading --line-number --color never '%s' %s")
           ((executable-find "pt")
            "pt -zS --nocolor --nogroup -e %s")
           (t counsel-grep-base-command))))
    (setq counsel-grep-base-command command))

  (when (executable-find "rg")
    (setq counsel-git-cmd "rg --files")
    (setq counsel-rg-base-command
          "rg -i -M 120 --no-heading --line-number --color never %s ."))

  (when (executable-find "fd")
    (defun cm/counsel-locate-cmd-fd (input)
      (format "fd --color never --hidden  -- \"%s\" /"
              (counsel--elisp-to-pcre
               (ivy--regex input t))))
    (setq counsel-locate-cmd 'cm/counsel-locate-cmd-fd))

  (defun cm/ivy-complete-subdirs ()
    (interactive)
    (let* ((dir ivy--directory)
           (default-directory dir)
           (map (make-sparse-keymap)))
      (define-key map (kbd "C-t")
        (lambda ()
          (interactive)
          (ivy-quit-and-run (counsel-find-file))))
      (ivy-read "Subdirs: "
                (process-lines "fd" "-aL" "--color" "never" "-t" "d")
                :action (lambda (result)
                          (ivy-quit-and-run
                            (let ((default-directory
                                   (file-name-as-directory result)))
                              (counsel-find-file))))
                :keymap map)))

  (bind-key "C-t" #'cm/ivy-complete-subdirs counsel-find-file-map))

;; (use-package ivy-rich
;;   :doc "Beautify ivy interface."
;;   :defines (all-the-icons-icon-alist
;;             all-the-icons-dir-icon-alist
;;             bookmark-alist)
;;   :functions (all-the-icons-icon-for-file
;;               all-the-icons-icon-for-mode
;;               all-the-icons-icon-family
;;               all-the-icons-match-to-alist
;;               all-the-icons-faicon
;;               all-the-icons-octicon
;;               all-the-icons-dir-is-submodule)
;;   :preface
;;   (defun cm/ivy-rich-bookmark-name (candidate)
;;     (car (assoc candidate bookmark-alist)))

;;   (defun cm/ivy-rich-buffer-icon (candidate)
;;     "Display buffer icons in `ivy-rich'."
;;     (when (display-graphic-p)
;;       (let* ((buffer (get-buffer candidate))
;;              (buffer-file-name (buffer-file-name buffer))
;;              (major-mode (buffer-local-value 'major-mode buffer))
;;              (icon (if (and buffer-file-name
;;                             (all-the-icons-match-to-alist buffer-file-name
;;                                                           all-the-icons-icon-alist))
;;                        (all-the-icons-icon-for-file (file-name-nondirectory buffer-file-name)
;;                                                     :height 0.9 :v-adjust -0.05)
;;                      (all-the-icons-icon-for-mode major-mode :height 0.9 :v-adjust -0.05))))
;;         (if (symbolp icon)
;;             (setq icon (all-the-icons-faicon "file-o" :face 'all-the-icons-dsilver :height 0.9 :v-adjust -0.05))
;;           icon))))

;;   (defun cm/ivy-rich-file-icon (candidate)
;;     "Display file icons in `ivy-rich'."
;;     (when (display-graphic-p)
;;       (let* ((path (concat ivy--directory candidate))
;;              (file (file-name-nondirectory path))
;;              (icon (cond ((file-directory-p path)
;;                           (cond
;;                            ((and (fboundp 'tramp-tramp-file-p)
;;                                  (tramp-tramp-file-p default-directory))
;;                             (all-the-icons-octicon "file-directory" :height 0.93 :v-adjust 0.01))
;;                            ((file-symlink-p path)
;;                             (all-the-icons-octicon "file-symlink-directory" :height 0.93 :v-adjust 0.01))
;;                            ((all-the-icons-dir-is-submodule path)
;;                             (all-the-icons-octicon "file-submodule" :height 0.93 :v-adjust 0.01))
;;                            ((file-exists-p (format "%s/.git" path))
;;                             (all-the-icons-octicon "repo" :height 1.0 :v-adjust -0.01))
;;                            (t (let ((matcher (all-the-icons-match-to-alist candidate all-the-icons-dir-icon-alist)))
;;                                 (apply (car matcher) (list (cadr matcher) :height 0.93 :v-adjust 0.01))))))
;;                          ((string-match "^/.*:$" path)
;;                           (all-the-icons-material "settings_remote" :height 0.9 :v-adjust -0.2))
;;                          ((not (string-empty-p file))
;;                           (all-the-icons-icon-for-file file :height 0.9 :v-adjust -0.05)))))
;;         (if (symbolp icon)
;;             (setq icon (all-the-icons-faicon "file-o" :face 'all-the-icons-dsilver :height 0.9 :v-adjust -0.05))
;;           icon))))
;;   :hook ((ivy-mode . ivy-rich-mode)
;;          (ivy-rich-mode . (lambda ()
;;                             (setq ivy-virtual-abbreviate
;;                                   (or (and ivy-rich-mode 'abbreviate) 'name)))))
;;   :init
;;   ;; For better performance
;;   (setq ivy-rich-parse-remote-buffer nil)

;;   (setq ivy-rich-display-transformers-list
;;         '(ivy-switch-buffer
;;           (:columns
;;            ((cm/ivy-rich-buffer-icon)
;;             (ivy-rich-candidate (:width 30))
;;             (ivy-rich-switch-buffer-size (:width 7))
;;             (ivy-rich-switch-buffer-indicators (:width 4 :face error :align right))
;;             (ivy-rich-switch-buffer-major-mode (:width 12 :face warning))
;;             (ivy-rich-switch-buffer-project (:width 15 :face success))
;;             (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
;;            :predicate
;;            (lambda (cand) (get-buffer cand)))
;;           ivy-switch-buffer-other-window
;;           (:columns
;;            ((cm/ivy-rich-buffer-icon)
;;             (ivy-rich-candidate (:width 30))
;;             (ivy-rich-switch-buffer-size (:width 7))
;;             (ivy-rich-switch-buffer-indicators (:width 4 :face error :align right))
;;             (ivy-rich-switch-buffer-major-mode (:width 12 :face warning))
;;             (ivy-rich-switch-buffer-project (:width 15 :face success))
;;             (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
;;            :predicate
;;            (lambda (cand) (get-buffer cand)))
;;           counsel-switch-buffer
;;           (:columns
;;            ((cm/ivy-rich-buffer-icon)
;;             (ivy-rich-candidate (:width 30))
;;             (ivy-rich-switch-buffer-size (:width 7))
;;             (ivy-rich-switch-buffer-indicators (:width 4 :face error :align right))
;;             (ivy-rich-switch-buffer-major-mode (:width 12 :face warning))
;;             (ivy-rich-switch-buffer-project (:width 15 :face success))
;;             (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
;;            :predicate
;;            (lambda (cand) (get-buffer cand)))
;;           persp-switch-to-buffer
;;           (:columns
;;            ((cm/ivy-rich-buffer-icon)
;;             (ivy-rich-candidate (:width 30))
;;             (ivy-rich-switch-buffer-size (:width 7))
;;             (ivy-rich-switch-buffer-indicators (:width 4 :face error :align right))
;;             (ivy-rich-switch-buffer-major-mode (:width 12 :face warning))
;;             (ivy-rich-switch-buffer-project (:width 15 :face success))
;;             (ivy-rich-switch-buffer-path (:width (lambda (x) (ivy-rich-switch-buffer-shorten-path x (ivy-rich-minibuffer-width 0.3))))))
;;            :predicate
;;            (lambda (cand) (get-buffer cand)))
;;           counsel-M-x
;;           (:columns
;;            ((counsel-M-x-transformer (:width 50))
;;             (ivy-rich-counsel-function-docstring (:face font-lock-doc-face))))
;;           counsel-describe-function
;;           (:columns
;;            ((counsel-describe-function-transformer (:width 50))
;;             (ivy-rich-counsel-function-docstring (:face font-lock-doc-face))))
;;           helpful-callable
;;           (:columns
;;            ((counsel-describe-function-transformer (:width 50))
;;             (ivy-rich-counsel-function-docstring (:face font-lock-doc-face))))
;;           counsel-describe-variable
;;           (:columns
;;            ((counsel-describe-variable-transformer (:width 50))
;;             (ivy-rich-counsel-variable-docstring (:face font-lock-doc-face))))
;;           helpful-variable
;;           (:columns
;;            ((counsel-describe-variable-transformer (:width 50))
;;             (ivy-rich-counsel-variable-docstring (:face font-lock-doc-face))))
;;           counsel-find-file
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-read-file-transformer)))
;;           counsel-file-jump
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-rich-candidate)))
;;           counsel-dired
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-read-file-transformer)))
;;           counsel-dired-jump
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-rich-candidate)))
;;           counsel-git
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-rich-candidate)))
;;           counsel-recentf
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-rich-candidate (:width 0.8))
;;             (ivy-rich-file-last-modified-time (:face font-lock-comment-face))))
;;           counsel-bookmark
;;           (:columns
;;            ((ivy-rich-bookmark-type)
;;             (cm/ivy-rich-bookmark-name (:width 40))
;;             (ivy-rich-bookmark-info)))
;;           counsel-projectile-switch-project
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-rich-candidate)))
;;           counsel-projectile-find-file
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (counsel-projectile-find-file-transformer)))
;;           counsel-projectile-find-dir
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (counsel-projectile-find-dir-transformer)))
;;           treemacs-projectile
;;           (:columns
;;            ((cm/ivy-rich-file-icon)
;;             (ivy-rich-candidate))))))

(use-package amx
  :doc "Enchanced M-x"
  :homepage "https://github.com/DarwinAwardWinner/amx")

(use-package flx
  :doc "Better fuzzy-searching candidates"
  :homepage "https://github.com/lewang/flx"
  :init
  (setq ivy-re-builders-alist
        '((counsel-grep   . ivy--regex-plus)
          ;; While `counsel-rg' don't have its own re-builder, it's inherited from `counsel-ag'
          (counsel-ag     . ivy--regex-plus)
          (counsel-rg     . ivy--regex-plus)
          (counsel-locate . ivy--regex-plus)
          (swiper         . ivy--regex-plus)
          (t              . ivy--regex-fuzzy))))

(use-package counsel-ffdata
  :bind
  ("C-c f f" . counsel-ffdata-firefox-bookmarks)
  ("C-c f h" . counsel-ffdata-firefox-history))

(provide 'init-ivy)
;; init-ivy.el ends here
