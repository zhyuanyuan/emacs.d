;;; -*- lexical-binding:t ; -*-

(use-package projectile
  :init
  (setq projectile-completion-system 'helm)
  (setq projectile-git-submodule-command nil)
  (when (and (memq system-type '(windows-nt ms-dos cygwin))
             (executable-find "rg"))
    (setq projectile-indexing-method 'alien))
  :after-call (pre-command-hook after-find-file dired-before-readin-hook)
  :bind
  ("C-$" . projectile-find-file))

(provide 'init-project)
;;; init-project.el ends here
