;;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'subr-x)
(require 'pcase)

(eval-and-compile
  (unless (>= emacs-major-version 26)
    ;; Define `if-let*' and `when-let*' variants for 25 users.
    (unless (fboundp 'if-let*) (defalias 'if-let* #'if-let))
    (unless (fboundp 'when-let*) (defalias 'when-let* #'when-let))))

(defun walle-enlist (elem)
  (if (and (listp elem)
           (not (functionp elem)))
      elem
    (list elem)))

(defmacro walle-with-gensyms (symbols &rest body)
  "Bind the SYMBOLS to fresh uninterned symbols and eval BODY."
  (declare (indent 1))
  `(let ,(mapcar (lambda (s)
                   `(,s (cl-gensym (symbol-name ',s))))
                 symbols)
     ,@body))

(eval-when-compile
  (defun walle--athread-process-forms (forms)
    (pcase forms
      (`nil
       'it)
      (`(,head . ,rest)
        `(let ((it ,head))
           ,(walle--athread-process-forms rest))))))

(defmacro walle-athread (initial &rest forms)
  "Anaphoric threading macros.

Each expression is bound to a temporarily variable called `it' which is
usable in next condition.

A implicit `nil' block is established around FORMS"
  (declare (debug t) (indent 1))
  `(let ((it ,initial))
     (cl-block nil ,(walle--athread-process-forms forms))))

(defmacro walle-import-from (package &rest funcs)
  (declare (indent 1) (debug (&rest symbolp)))
  `(progn
     ,@(mapcar (lambda (f)
                 `(declare-function ,f ,(symbol-name package)))
               funcs)))

(provide 'walle-utils)
;;; walle-utils.el ends here
