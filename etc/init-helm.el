;;; -*- lexical-binding: t; -*-

(use-package helm
 :defer 1
 :after-call post-command-hook
 :init
   (setq helm-describe-function-function 'helpful-callable)
   (setq helm-inherit-input-method nil)
 :bind
   ([remap apropos] . helm-apropos)
   ([remap find-library] . helm-locate-library)
   ([remap bookmark-jump] . helm-bookmarks)
   ([remap execute-extended-command] . helm-M-x)
   ([remap find-file] . helm-find-files)
   ([remap imenu-anywhere] . helm-imenu-anywhere)
   ([remap imenu] . helm-semantic-or-imenu)
   ([remap noop-show-kill-ring] . helm-show-kill-ring)
   ;; ([remap persp-switch-to-buffer] . +helm/workspace-mini)
   ([remap switch-to-buffer] . helm-mini)
   ;; ([remap projectile-find-file] . +helm/projectile-find-file)
   ([remap projectile-recentf] . helm-projectile-recentf)
   ([remap projectile-switch-project] . helm-projectile-switch-project)
   ([remap projectile-switch-to-buffer] . helm-projectile-switch-to-buffer)
   ([remap recentf-open-files] . helm-recentf)
   ([remap yank-pop] . helm-show-kill-ring)
   ([remap woman] . helm-man-woman)
   ([remap comint-history-isearch-backward-regexp] . helm-comint-input-ring)
   ([remap comint-history-isearch-backward] . helm-comint-input-ring)
   ("C-c C-r" . helm-resume)
   (:map minibuffer-local-map
         ("C-r" . helm-minibuffer-history)
         ("C-c C-l" . helm-minibuffer-history))
 :config
   (helm-mode 1)
   (add-to-list 'helm-completing-read-handlers-alist
                `(,#'find-file-at-point . nil)))

(use-package helm-grep
 :init
   (setq helm-grep-ag-command
         "rg --color=always --no-heading --line-number %s %s %s")
 :bind
   ("C-4" . helm-do-grep-ag))

(use-package helm-files
 :bind
   (:map helm-find-files-map
         ("C-s" . helm-ff-run-grep-ag)))

(use-package helm-swoop
 :custom-face
   (helm-swoop-line-number-face ((t :inherit line-number)))
 :init
   (setq helm-swoop-split-with-multiple-windows t
         helm-swoop-split-direction 'split-window-vertically
         helm-swoop-speed-or-color t
         helm-swoop-split-window-function 'helm-default-display-buffer
         helm-swoop-pre-input-function (lambda () "")
         helm-swoop-flash-region-function 'pulse-momentary-highlight-region
         helm-swoop-use-line-number-face t)
 :bind
   ("C-s" . helm-swoop)
   (:map helm-swoop-map
         ([remap next-history-element] . helm-swoop-yank-thing-at-point))
   (:map helm-multi-swoop-map
         ([remap next-history-element] . helm-swoop-yank-thing-at-point)))

(use-package helm-buffers
 :init
   (setq helm-buffer-max-length 60)
 :bind
   ("C-1" . walle-helm-enh-mini))

(use-package helm-info
 :init
   (setq helm-info-default-sources
         '(helm-source-info-elisp
           helm-source-info-cl
           helm-source-info-org))
 :bind
   ("C-c h" . helm-info-at-point))

(use-package ace-jump-helm-line
 :after helm
 :demand
 :bind
   (:map helm-map
         ("C-'" . ace-jump-helm-line-and-select)))

(use-package helm-fuz
 :demand t
 :after helm
 :config
   (unless (require 'fuz-core nil t)
     (fuz-build-and-load-dymod))
   (helm-fuz-mode)
   (setq helm-M-x-fuzzy-match t
         helm-apropos-fuzzy-match t
         helm-bookmark-show-location t
         helm-buffers-fuzzy-matching t
         helm-completion-in-region-fuzzy-match t
         helm-completion-in-region-fuzzy-match t
         helm-ff-fuzzy-matching t
         helm-file-cache-fuzzy-match t
         helm-lisp-fuzzy-completion t
         helm-locate-fuzzy-match t
         helm-mode-fuzzy-match t
         helm-recentf-fuzzy-match t
         helm-projectile-fuzzy-match t))

(provide 'init-helm)
;;; init-helm.el ends here
