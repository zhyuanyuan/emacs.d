;;; -*- lexical-binding:t ; -*-

;; Directory operations
(use-package dired
 :init
   ;; Always delete and copy recursively
   (setq dired-recursive-deletes 'always
         dired-recursive-copies 'always
         dired-dwim-target t)
 :config
   (use-package diredfl
    :doc "Colorize your `dired'"
    :commands diredfl-global-mode
    :init (diredfl-global-mode)
    :config)

   (use-package dired-x
    :demand t
    :init
      (let ((cmd "start"))
        (setq dired-guess-shell-alist-user
              `(("\\.pdf\\'" ,cmd)
                ("\\.docx\\'" ,cmd)
                ("\\.\\(?:djvu\\|eps\\)\\'" ,cmd)
                ("\\.\\(?:jpg\\|jpeg\\|png\\|gif\\|xpm\\)\\'" ,cmd)
                ("\\.\\(?:xcf\\)\\'" ,cmd)
                ("\\.csv\\'" ,cmd)
                ("\\.tex\\'" ,cmd)
                ("\\.\\(?:mp4\\|mkv\\|avi\\|flv\\|rm\\|rmvb\\|ogv\\)\\(?:\\.part\\)?\\'"
                 ,cmd)
                ("\\.\\(?:mp3\\|flac\\)\\'" ,cmd)
                ("\\.html?\\'" ,cmd)
                ("\\.md\\'" ,cmd))))

    :config
      (setq dired-omit-files
            (concat dired-omit-files
                    "\\|^.DS_Store$\\|^.projectile$\\|^.git*\\|^.svn$\\|^.vscode$\\|\\.js\\.meta$\\|\\.meta$\\|\\.elc$\\|^.emacs.*")))

   (use-package wdired
    :bind
      (:map dired-mode-map
            ("C-c C-w" . wdired-change-to-wdired-mode)))

   (use-package dired-ranger
    :init
      (setq dired-ranger-copy-ring-size 200)
    :bind
      (:map dired-mode-map
            ("; C" . dired-ranger-copy)
            ("; y" . dired-ranger-paste)
            ("; R" . dired-ranger-move)))

   (use-package dired-narrow
    :bind
      (:map dired-mode-map
            ("; n" . dired-narrow-fuzzy)
            ("; N" . dired-narrow)))

   (use-package dired-filter
    :demand
    :init
      (setq dired-filter-prefix "\\"))

   (use-package dired-open :demand)

 :bind
   (:map dired-mode-map
         ("j" . dired-next-line)
         ("k" . dired-previous-line)
         ("h" . dired-up-directory)
         ("l" . dired-find-file)
         ("f" . dired-goto-file)
         ("K" . dired-kill-line)
         ("r" . dired-do-redisplay)))
(provide 'init-dired)
;;; init-dired ends here
