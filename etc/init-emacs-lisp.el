;;; -*- lexical-binding:t ; -*-

(use-package elisp-mode
  :doc "The core mode of hacking Emacs!"
  :hook (emacs-lisp-mode . (lambda ()
                             (setq-local mode-name "Elisp")
                             (setq-local outline-regexp ";;;;* [^ \t\n]")))
  :bind
  (:map emacs-lisp-mode-map
        ("C-c C-c" . eval-defun)
        ("C-c C-b" . eval-buffer))
  :config
  ;; (defvar cm//emacs-lisp-face nil)

  (elispfl-mode))

(use-package ielm
  :doc "Inferior Emacs-lisp mode"
  :hook
  (ielm-mode . turn-on-smartparens-strict-mode)
  :preface
  (defun cm/ielm ()
    "Open the Emacs Lisp REPL (`ielm')."
    (interactive)
    (pop-to-buffer
     (or (get-buffer "*ielm*")
         (progn (ielm)
                (let ((buf (get-buffer "*ielm*")))
                  (bury-buffer buf)
                  buf)))))
  (defun cm/ielm-cls ()
    (interactive)
    (let ((inhibit-read-only t))
      (erase-buffer))
    (ielm-return)
    (save-excursion
      (goto-char (point-min))
      (insert ielm-header)))
  :bind
  (:map emacs-lisp-mode-map
        ("C-c '" . cm/ielm))
  (:map lisp-interaction-mode-map
        ("C-c '" . cm/ielm))
  (:map ielm-map
        ("C-l" . cm/ielm-cls))
  :config
  (elispfl-ielm-mode)
  )

(use-package macrostep
  :doc "Interactive macro expansion tool."
  :hook (before-save . macrostep-collapse-all)
  :bind
  (:map emacs-lisp-mode-map
        ("C-c C-e" . macrostep-expand))
  (:map lisp-interaction-mode-map
        ("C-c C-e" . macrostep-expand))
  (:map macrostep-keymap
        ("J" . macrostep-next-macro)
        ("K" . macrostep-prev-macro)
        ("e" . macrostep-expand)
        ("c" . macrostep-collapse)
        ("q" . macrostep-collapse-all)))

(use-package sly-el-indent
  :hook (emacs-lisp-mode . sly-el-indent-setup))

(use-package cl-indent
  :disabled
  :after elisp-mode
  :demand
  :doc "Better indentation mode."
  :config
  (setq lisp-indent-function #'common-lisp-indent-function)
  (defvar cm/correct-indentation-list
    '((defface . nil)
      (defalias . nil)
      (define-minor-mode . 1)
      (define-derived-mode . 3)
      (defface . 1)
      ;; (unwind-protect . 1)
      (define-globalized-minor-mode . nil)
      ;; Fix `use-pacakge' indentation.
      (use-package . 1)))
  (pcase-dolist (`(,sym . ,spec) cm/correct-indentation-list)
    (put sym 'common-lisp-indent-function-for-elisp spec)))

(provide 'init-emacs-lisp)
;;; init-emacs-lisp.el ends here
