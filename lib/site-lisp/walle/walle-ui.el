;;; -*- lexical-binding: t; -*-

(require 'advice)
(require 'walle-utils)

(defvar walle-ui--after-display-init-functions nil
  "Functions should be run after display system was fully initialized.")

(defadvice server-create-window-system-frame
    (after walle-ui--init-display-advice! activate)
  "After Emacs server creates a frame, run functions queued in
`walle-ui--after-display-init-functions' to do any setup that needs to have
the display system initialized."
  (mapc #'funcall (reverse walle-ui--after-display-init-functions))
  (ad-disable-advice 'server-create-window-system-frame
                     'after
                     'walle-ui--init-display-advice!)
  (ad-activate 'server-create-window-system-frame))

(defun walle-ui--display-initialized? ()
  (cond ((boundp 'x-initialized)
         x-initialized)
        ((boundp 'w32-initialized)
         (font-family-list))
        ((boundp 'ns-initialized)
         ns-initialized)
        (t (display-graphic-p))))

(defsubst walle-ui-display-color-emoji? ()
  "Return non-nil if emacs can display color emoji.

Notice that this function assume you have graphics display."
  (or (and (>= emacs-major-version 27)
           (featurep 'cairo)
           (memq 'ftcrhb (frame-parameter (selected-frame) 'font-backend)))
      (featurep 'cocoa)))

;;;###autoload
(defmacro walle-ui-eval-after-display-init (&rest body)
  (declare (indent 0) (debug t))
  (walle-with-gensyms (init)
    `(let ((,init (walle-ui--display-initialized?)))
       (if (and (daemonp)
                (not ,init))
           (push (lambda () ,@body) walle-ui--after-display-init-functions)
         ,@body))))

(provide 'walle-ui)
;;; walle-ui.el ends here
