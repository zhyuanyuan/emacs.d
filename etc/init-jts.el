;;; -*- lexical-binding:t ; -*-

(use-package js-mode
  :init
  ;; Follow standardjs spec
  (setq js-indent-level 4))

;;* Javascript
;; (use-package js2-mode
;;   :mode "\\.m?js\\'"
;;   :interpreter "node"
;;   :config
;;   ;; Flymake do this
;;   (setq js2-mode-show-parse-errors nil
;;         js2-mode-show-strict-warnings nil
;;         js2-strict-trailing-comma-warning nil
;;         js2-strict-missing-semi-warning nil)
;;   ;; Maximum highlight
;;   (setq js2-highlight-level 3
;;         js2-highlight-external-variables t))

(use-package js-comint
  :bind
  (:map js-mode-map
        ("C-c '" . js-comint-repl)))

(use-package prettier-js)

;; (use-package js2-refactor)

;;* Typescript
(use-package typescript-mode
  :mode "\\(\\.d\\)?\\.ts\\'")

;;* JSON
(use-package json-mode)

(provide 'init-jts)
;;; init-jts.el ends here
