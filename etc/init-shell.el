;;; -*- lexical-binding:t ; -*-

;; Use msys2 bash shell
;; (setq explicit-shell-file-name "bash.exe")

(setq explicit-shell-file-name "zsh")

(use-package sh-script
  :init
  (add-to-list 'auto-mode-alist '("/PKGBUILD\\'" . sh-mode))
  (walle-electric-set! 'sh-mode
    :words '("else" "elif" "fi" "done" "then" "do" "esac" ";;")))

;;* Aweshell
(use-package aweshell
  :ensure nil
  :commands (aweshell-toggle)
  :init
  (cm/add-temp-hook #'eshell
    (unless (featurep 'aweshell)
      (require 'aweshell)))
  :config
  (setq eshell-prompt-function 'epe-theme-lambda)
  :bind
  ("H-e" . aweshell-toggle))

(use-package with-editor
  :bind
  ([remap async-shell-command] . with-editor-async-shell-command)
  ([remap shell-command] . with-editor-shell-command))

(provide 'init-shell)
;;; init-shell.el ends here
