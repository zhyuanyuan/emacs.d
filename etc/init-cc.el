;;; -*- lexical-binding: t; -*-

(use-package cc-mode
  :config
  ;; Use smartparens to handle electric parens
  (c-toggle-electric-state -1)
  (c-toggle-auto-newline -1)
  (setq c-electric-flag nil)
  (dolist (key '("#" "{" "}" "/" "*" ";" "," ":" "(" ")" "\177"))
    (define-key c-mode-base-map key nil))
  (with-eval-after-load 'smartparens
    (sp-with-modes '(c-mode c++-mode objc-mode java-mode)
      (sp-local-pair "/*!" "*/"
                     :post-handlers '(("||\n[i]" "RET") ("[d-1]< | " "SPC"))))))

(use-package rust-mode
  :init
  (walle-electric-set! 'rust-mode
    :words '("->" "where"))) 

(provide 'init-cc)
;;; init-cc.el ends here
