;;; al-collector.el --- Collect all autoloads in one file  -*- lexical-binding: t -*-

;; Copyright (C) 2019 Zhu Zihao

;; Author: Zhu Zihao <all_but_last@163.com>
;; URL: https://framagit.org/citreu/emacs.d
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1"))
;; Keywords: autoload

;; This file is NOT part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cl-lib)
(require 'autoload)
(require 'limon)
(eval-when-compile (require 'subr-x))

;; Some packages using `names' have different way to generate autoload,
;; so we require it here to ensure we can generate correct autoloads for them.
(require 'names nil t)

(defvar al-collector-loaddefs-local-variables-alist
  '((version-control . never)
    (no-byte-compile . t)
    (no-update-autoloads . t)
    (coding . utf-8))
  "An alist of local variables for generated loaddefs file.")

(defvar al-collector-loaddefs-header-format
  "\
;;; %s --- Automatically extracted Emacs packages loaddefs
;;
;;; Code:

"
  "The header format of generated loaddefs file.

Slot will be filled with the basename of generated loaddefs.")

(defun al-collector--collect-subdir! (dir)
  "Collect all subdirectories in DIR.

Sign: (-> Str (Listof Str))

Dot-directories and directories contain `.nosearch' will be skipped."
  (thread-last (directory-files dir nil)
    (cl-remove-if (lambda (f)
                    (string-prefix-p "." f)))
    (mapcar (lambda (d) (expand-file-name d dir)))
    (cl-remove-if-not #'file-directory-p)
    (cl-remove-if (lambda (d)
                    (file-exists-p (expand-file-name ".nosearch"
                                                     d))))))

(defun al-collector--collect-el-recursively! (dir)
  "Collect all `.el' files in DIR recursively.

Sign: (-> Str (Listof Str))"
  (let ((elfiles (limon-libraries-in-dir dir limon-nonlibrary-patterns))
        (subdir (al-collector--collect-subdir! dir)))
    (nconc elfiles
           (mapcan #'al-collector--collect-el-recursively! subdir))))

(defun al-collector--format-local-variables (local-var-alist)
  "Format file local variables definition according to LOCAL-VAR-ALIST."
  (concat ";; Local Variables:\n"
          (mapconcat (cl-function
                      (lambda ((var . val))
                        (format ";; %s: %s"
                                var val)))
                     local-var-alist
                     "\n")
          "\n"
          ";; End:\n"))

(defun al-collector--insert-autoloads! (file &optional full-name?)
  "Generate autloads from FILE and insert it to current buffer.

If FULL-NAME?, use the absolute path of each elisp file,
otherwise use only the name.
"
  (let* ((name-fn (if full-name? #'file-name-sans-extension #'file-name-base))
         (generated-autoload-load-name (funcall name-fn file)))
    (autoload-generate-file-autoloads file (current-buffer))))

;;;###autoload
(defun al-collector-generate-autoloads! (dir target &optional full-name?)
  "Generate loaddefs recursively for all package in DIR to file TARGET.

Sign: (-> Str Str Nil)

If FULL-NAME?, use the absolute path of each elisp file,
otherwise use only the name.

Notice that elisp file in dotdir or dir with `.nosearch' file will be ignored."
  (let ((target-fname (file-name-nondirectory target)))
    (with-temp-file target
      (insert (format al-collector-loaddefs-header-format target-fname))
      (dolist (f (al-collector--collect-el-recursively! dir))
        (let ((real-f (file-truename f)))
          (al-collector--insert-autoloads! real-f full-name?)))
      (insert
       (format "\

\(provide '%s)

%s

;;; %s ends here"
               (file-name-base target)
               (al-collector--format-local-variables
                al-collector-loaddefs-local-variables-alist)
               target-fname)))))

;;;###autoload
(defun al-collector-generate-autoloads-incrementally!
    (dir target &optional full-name?)
  "Generate autoload from packages in DIR, insert them to TARGET incrementally."
  (with-temp-file target
    (insert-file-contents-literally target)
    (goto-char (point-max))
    (search-backward "" nil t)
    (dolist (f (al-collector--collect-el-recursively! dir))
      (let ((real-f (file-truename f)))
        (al-collector--insert-autoloads! real-f full-name?)))))


(provide 'al-collector)

;; Local Variables:
;; coding: utf-8
;; End:

;;; al-collector.el ends here
