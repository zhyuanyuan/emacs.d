;;; -*- lexical-binding: t; -*-

;;;###autoload
(defun walle-files-sudo-find-file (file)
  "Open FILE as root."
  (interactive
   (list (read-file-name "Open as root: ")))
  (when (file-writable-p file)
    (user-error "File is user writeable, aborting sudo"))
  (find-file (if (file-remote-p file)
                 (concat "/" (file-remote-p file 'method)
                         ":" (file-remote-p file 'user)
                         "@" (file-remote-p file 'host)
                         "|sudo:root@" (file-remote-p file 'host)
                         ":" (file-remote-p file 'localname))
               (concat "/sudo:root@localhost:" file))))

;;;###autoload
(defun walle-files-sudo-this-file ()
  "Open the current file as root."
  (interactive)
  (let ((buf (current-buffer)))
    (walle-files-sudo-find-file (or dired-directory
                                    (file-truename buffer-file-name)))
    (kill-buffer buf)))

(provide 'walle-files)
;;; walle-files.el ends here
