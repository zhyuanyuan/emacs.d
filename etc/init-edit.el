;;; -*- lexical-binding:t ; -*-

;;* Misc
;; Explicitly set the prefered coding systems to avoid annoying prompt
;; from emacs (especially on Microsoft Windows)
(prefer-coding-system 'utf-8)

(setq uniquify-buffer-name-style 'post-forward-angle-brackets) ; Show path if names are same
(setq adaptive-fill-regexp "[ t]+|[ t]*([0-9]+.|*+)[ t]*")
(setq adaptive-fill-first-line-regexp "^* *$")
(setq delete-by-moving-to-trash t)         ; Deleting files go to OS's trash folder
(setq make-backup-files nil)               ; Forbide to make backup files
(setq auto-save-default nil)               ; Disable auto save
(setq set-mark-command-repeat-pop t)       ; Repeating C-SPC after popping mark pops it again
(setq-default kill-whole-line t)           ; Kill line including '\n'

(setq sentence-end "\\([。！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")
(setq sentence-end-double-space nil)
(add-hook 'after-init-hook 'column-number-mode)

(use-package text-mode
  :init
  (setq-default major-mode 'text-mode)
  :hook (text-mode . turn-on-auto-fill)
  :hook (text-mode . (lambda ()
                       (setq-local cursor-type '(bar . 2)))))


;; Automatically revert buffers
(use-package autorevert
  :hook (after-init . global-auto-revert-mode)
  :bind
  ("C-S-r" . revert-buffer))

;;* Auto-saving
(setq save-silently t)
(setq auto-save-default nil
      auto-save-list-file-prefix (expand-file-name
                                  "auto-save-list/.save-"
                                  cm/cache-files-directory))

(use-package goto-addr
  :doc "Click to browse URL or to send to e-mail address."
  :hook ((text-mode . goto-address-mode)
         (prog-mode . goto-address-prog-mode)))

(use-package auto-save
  :doc "Save files automatically."
  :commands auto-save-enable
  :init
  (setq auto-save-silent t)
  :after-call (after-find-file mouse-leave-buffer-hook)
  :config
  (auto-save-enable))

;;* Visualize Undo Operation
(use-package undo-tree
  :after-call (after-find-file mouse-leave-buffer-hook)
  :init
  (setq undo-tree-visualizer-diff t)
  (setq undo-tree-enable-undo-in-region nil)
  (setq undo-tree-auto-save-history t
        undo-tree-history-directory-alist
        `(("." .
           ,(expand-file-name "undo-tree-hist/" cm/cache-files-directory))))
  :config
  ;; Compress undo history with xz or gzip
  (defun cm/undo-tree-make-history-save-file-name (file)
    (cond ((executable-find "zstd") (concat file ".zst"))
          ((executable-find "gzip") (concat file ".gz"))
          (file)))

  (advice-add #'undo-tree-make-history-save-file-name :filter-return
              #'cm/undo-tree-make-history-save-file-name)

  (defun cm/strip-text-properties-from-undo-history (&rest _)
    (dolist (item buffer-undo-list)
      (and (consp item)
           (stringp (car item))
           (setcar item (substring-no-properties (car item))))))

  (advice-add #'undo-list-transfer-to-tree :before #'cm/strip-text-properties-from-undo-history)

  (defun cm/compress-undo-tree-history (orig-fn &rest args)
    (cl-letf* ((jka-compr-verbose nil)
               (old-write-region (symbol-function #'write-region))
               ((symbol-function #'write-region)
                (lambda (start end filename &optional append _visit &rest args)
                  (apply old-write-region start end filename append 0 args))))
      (apply orig-fn args)))

  (advice-add #'undo-tree-save-history :around #'cm/compress-undo-tree-history)

  (defun cm/reopen-diff-next-undo-visualize ()
    (setq undo-tree-visualizer-diff t))

  (advice-add #'undo-tree-visualizer-quit :after #'cm/reopen-diff-next-undo-visualize)

  (global-undo-tree-mode)
  :bind
  (:map undo-tree-visualizer-mode-map
        ("j" . undo-tree-visualize-redo)
        ("k" . undo-tree-visualize-undo)
        ("h" . undo-tree-visualize-switch-branch-left)
        ("l" . undo-tree-visualize-switch-branch-right)))

;; (use-package undo-propose
;;   :after-call (after-find-file mouse-leave-buffer-hook)
;;   :config
;;   (undo-propose-mode))

;;* Visualize searching
(use-package anzu
  :bind
  (([remap query-replace] . anzu-query-replace)
   ([remap query-replace-regexp] . anzu-query-replace-regexp)
   ([remap isearch-query-replace] . anzu-isearch-query-replace)
   ([remap isearch-query-replace-regexp] . anzu-isearch-query-replace-regexp)))

(use-package replace
  :bind
  ("M-R" . query-replace-regexp))

(use-package symbol-overlay
  :doc "Highlight the symbol over the cursor."
  :bind
  ("C-r" . symbol-overlay-put)
  (:map symbol-overlay-map
        ("j" . symbol-overlay-jump-next)
        ("k" . symbol-overlay-jump-prev)))

(use-package avy
  :doc "Fast tree-like jumping and navigation"
  :homepage "https://github.com/abo-abo/avy"
  :bind
  (([remap goto-line] . avy-goto-line)
   ("C-." . avy-goto-word-1))
  :init
  (setq avy-timeout-seconds 0.3         avy-background nil
        avy-all-windows nil)
  (setq avy-keys '(?a ?s ?d ?f ?j ?k ?l ?\; ?w ?e ?i ?o ?n ?v ?m ?c))
  :config
  (avy-setup-default))

(use-package mwim
  :bind
  (([remap move-beginning-of-line] . mwim-beginning-of-code-or-line)
   ([remap move-end-of-line] . mwim-end-of-code-or-line)))

(use-package ediff
  :init
  (defun cm/ediff-split-window-fn (&rest args)
    (if (> (frame-width) 150)
        (apply #'split-window-horizontally args)
      (apply #'split-window-vertically args)))

  (setq ediff-window-setup-function 'ediff-setup-windows-plain
        ediff-split-window-function #'cm/ediff-split-window-fn))

(use-package bug-reference
  :hook (prog-mode . bug-reference-prog-mode))

(use-package whitespace
  :doc "Cleanup bad whitespace."
  :hook ((prog-mode conf-mode) . whitespace-mode)
  :init
  (setq whitespace-line-column fill-column
        whitespace-action nil
        whitespace-style '(
                           face lines-tail
                           trailing space-before-tab
                           indentation empty space-after-tab))
  :bind
  ("C-2" . whitespace-cleanup))

;;* Smartparens
(use-package smartparens
  :defer 1
  :after-call pre-command-hook
  :init
  (defvar cm/enable-sp-in-minibuffer-commands
    '(eval-expression eldoc-eval-expression)
    "Enable `smartparens' in some minibuffer-based commnads")
  (setq sp-base-key-bindings 'sp)
  ;; Don't need this, annoying.
  (setq sp-echo-match-when-invisible nil)
  ;; Enable paren highlighting in minibuffer
  (setq sp-ignore-modes-list nil)
  :hook (prog-mode . turn-on-smartparens-strict-mode)
  :hook (minibuffer-setup . turn-on-show-smartparens-mode)
  :config
  (require 'smartparens-config)
  (smartparens-global-mode)
  (show-smartparens-global-mode)
  ;; Reset Keybindings
  (bind-key "M-<backspace>" #'sp-backward-kill-word smartparens-mode-map)


  (defun cm/enable-sp-in-minibuffer ()
    "Enable `smartparens' in minibuffer sometimes"
    (when (memq this-command cm/enable-sp-in-minibuffer-commands)
      (smartparens-mode)))
  (add-hook 'minibuffer-setup-hook #'cm/enable-sp-in-minibuffer)
  (sp-local-pair 'minibuffer-inactive-mode "'" nil :actions nil)

  ;; Fix overlay problem with yasnippet
  (with-eval-after-load 'yasnippet
    (advice-add #'yas-expand :before #'sp-remove-active-pair-overlay))

  ;; Electric brackets
  (dolist (brace '("(" "{" "["))
    (sp-pair brace nil
             :post-handlers '(("||\n[i]" "RET")
                              ("| " "SPC"))
             :unless '(sp-point-before-word-p sp-point-before-same-p))))

;;* Region Operation
;; Expand-region
(use-package expand-region
  :bind
  ("C->" . er/expand-region))

(use-package multiple-cursors
  :bind
  (("C-S-j" . mc/mark-next-like-this)
   ("C-S-k" . mc/mark-previous-like-this)
   ("C-c C-S-j" . mc/mark-all-like-this)))

;; Useful functions

(defun cm/zip-blank-line-region (beg end)
  (interactive "r")
  (save-excursion
    (goto-char beg)
    (while (re-search-forward (rx (1+ bol
                                      (0+ whitespace)
                                      (or "\n" "\r" "\r\n")
                                      (0+ whitespace)
                                      eol))
                              end
                              t)
      (replace-match "" nil t))))

(provide 'init-edit)

;;; init-edit.el ends here
