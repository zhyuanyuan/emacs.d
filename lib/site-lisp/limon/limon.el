;;; limon.el --- Extra lisp file maintenance tool  -*- lexical-binding: t -*-

;; Copyright (C) 2019 Zhu Zihao

;; Author: Zhu Zihao <all_but_last@163.com>
;; URL: https://github.com/cireu/limon
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1"))
;; Keywords: lisp

;; This file is NOT part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


(require 'cl-lib)
(require 'pcase)
;; (require 'lisp-mnt)
(require 'memoize)
(require 'package)

(autoload 'lm-with-file "lisp-mnt" nil nil t)


(defconst limon-builtin-packages-list
  (mapcar #'car package--builtin-versions)
  "All name of builtin packages.")

(defconst limon-el-suffixes (let ((load-suffixes '(".el")))
                              (get-load-suffixes)))

(defun limon--do-sexps-in-file (file func)
  "Call FUNC with each sexp in FILE."
  (lm-with-file file
    (cl-loop for sexp = (condition-case nil
                            (read (current-buffer))
                          (end-of-file (cl-return nil)))
             do (funcall func sexp))))

(defmacro limon-do-matched-sexps (file pat &rest body)
  "Eval BODY on each sexp matched by PAT in FILE."
  (declare (indent 2) (debug (form pcase-PAT body)))
  (let ((arg (make-symbol "arg")))
    `(limon--do-sexps-in-file
      ,file
      (lambda (,arg)
        (pcase ,arg
          (,pat
           ,@body))))))

(defmacro limon-with-first-matched (file pat &rest body)
  "Find the first sexp matched by PAT in FILE, yield BODY."
  (declare (indent 2) (debug (form pcase-PAT body)))
  (macroexp-let2 nil return '(make-symbol "return")
    `(catch ,return
       (limon-do-matched-sexps ,file ,pat
         (throw ,return (progn ,@body))))))

(defmemoize limon--make-match-regexp (wildcards exts)
  "Cache the result of `wildcard-to-regexp'."
  (let ((results nil))
    (dolist (wc wildcards)
      (dolist (ext exts)
        (push (concat wc ext) results)))
    (mapconcat #'wildcard-to-regexp results "\\|")))

(defvar limon-default-nonlibrary-patterns
  '("test" "tests"
    "*-test" "*-tests" "test-*"
    ".dir-locals")
  "Default filename pattern indicates a non-library `.el' file.")

(defun limon-libraries-in-dir (dir exclude-pattern)
  "Search libraries indicated by EXCLUDE-PATTERN in DIR.

EXCLUDE-PATTERN can be a list of wildcard patterns, which will be used to
match against the basename of `.el' files, the file will be excluded
if one of patterns matched successfully."
  (let* ((exts limon-el-suffixes)
         (libs (directory-files dir nil (concat (regexp-opt exts) "\\'"))))
    (if exclude-pattern
        (let ((excludes-re (limon--make-match-regexp exclude-pattern exts))
              (default-directory dir))
          (mapcar #'expand-file-name
                  (cl-remove-if (lambda (it)
                                  (string-match-p excludes-re it))
                                libs)))
      libs)))

(cl-defun limon-all-libraries (&optional
                                 (dirs load-path)
                                 (exclude-pattern
                                  limon-default-nonlibrary-patterns))
  "Search all libraries indicated by EXCLUDE-PATTERN in DIRS."
  (cl-mapcan (lambda (it)
               (limon-libraries-in-dir it exclude-pattern))
             dirs))

(defun limon-required (&optional file)
  (let ((soft nil) (hard nil))
    (cl-labels ((scan-requirement (form)
                  (pcase form
                    (`(require ',pkg . ,optional)
                      (pcase-let ((`(,_ ,noerror) optional))
                        (push pkg (if noerror soft hard))))
                    ((and `(,head . ,body)
                          (guard (memq head '(progn
                                              eval-and-compile
                                              eval-when-compile))))
                     (ignore head)
                     (mapc #'scan-requirement body)))))
      (limon--do-sexps-in-file file #'scan-requirement))
    (list hard soft)))

(defun limon-provided (&optional file)
  (limon-with-first-matched file (or `(provide ',feat)
                                     `(provide-theme ',feat))
    feat))


(provide 'limon)

;; Local Variables:
;; coding: utf-8
;; End:

;;; limon.el ends here
