;;; -*- lexical-binding: t; -*-

(use-package org
  :doc "Orgnize your plain life in plain text."
  :init
  ;; (when (daemonp)
  ;;   (run-with-idle-timer 10 nil (lambda ()
  ;;                                 (require 'org))))
  (defvar cm/org-directory (expand-file-name "~/org"))
  (defvar org-agenda-files (list cm/org-directory))
  (defvar cm/org-plus-contrib-directory (expand-file-name "org-plus-contrib/lisp"
                                                          cm/site-lisp-directory))
  :config
  ;; `doom-themes' integration
  (doom-themes-org-config)
  ;; Load `org-plus-contrib'
  (dolist (f (cl-remove-duplicates
              (mapcar #'file-name-sans-extension
                      (directory-files cm/org-plus-contrib-directory
                                       'full
                                       "\\(?:\\.elc?\\)\\'"))
              :test #'equal))
    (ignore-errors (load f nil 'no-message)))
  :hook (org-mode . (lambda ()
                      (setq truncate-lines t))))

(use-package org-capture
  :bind
  ("<f6>" . org-capture)
  :init
  (defvar cm/org-capture-todo-file (expand-file-name "todo.org" cm/org-directory))
  (defvar cm/org-capture-notes-file (expand-file-name "notes.org" cm/org-directory))

  (defun cm//org-capture-root (path)
    (require 'projectile)
    (let ((filename (concat "orgc/" (file-name-nondirectory path))))
      (expand-file-name
       filename
       (or (locate-dominating-file (file-truename default-directory)
                                   filename)
           (projectile-project-root)
           (user-error "Couldn't detect a project")))))

  (defun cm/org-capture-project-todo-file ()
    "Find the nearest `cm/org-capture-todo-file' in a parent directory, otherwise,
opens a blank one at the project root. Throws an error if not in a project."
    (cm//org-capture-root cm/org-capture-todo-file))

  (defun cm/org-capture-project-notes-file ()
    "Find the nearest `cm/org-capture-notes-file' in a parent directory, otherwise,
opens a blank one at the project root. Throws an error if not in a project."
    (cm//org-capture-root cm/org-capture-notes-file))

  (defvar org-capture-templates
    '(("t" "Task" entry
       (file+headline cm/org-capture-todo-file "Tasks")
       "* TODO %? %U %^G\nDEADLINE: %^T\n%i\n%a")
      ("n" "Notes" entry
       (file+weektree cm/org-capture-notes-file "Notes")
       "* %<[%H:%M]> %^{Title} %^G\n%i%?\n%a")

      ("p" "Templates for projects")
      ("pt" "Project todo" entry
       (file+headline cm/org-capture-project-todo-file "Tasks")
       "* TODO %T %? %^G\nDEADLINE: %^T\n%i\n%a")
      ("pn" "Project notes" entry
       (file+headline cm/org-capture-project-notes-file "Notes")
       "* %<[%H:%M]> %^{Title} %^G\n%i%?\n%a")

      ("x" "Templates for Org Protocol")
      ("xp" "Protocol" entry
       (file+weektree cm/org-capture-notes-file "Notes")
       "* %<[%H:%M]> %^{Title} %^G\n#+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")
      ("xl" "Protocol Link" entry
       (file+headline cm/org-capture-notes-file "Notes")
       "* %<[%H:%M]> %? %^G\n[[%:link][%:description]]")))

  (defvar org-default-notes-file)
  :config
  (unless org-default-notes-file
    (setq org-default-notes-file cm/org-capture-notes-file))

  (defun cm/org-expand-variable-path (file)
    "If a variable is used for a file path in `org-capture-template', it is used
as is, and expanded relative to `default-directory'. This changes it to be
relative to `cm/org-directory', unless it is an absolute path."
    (if (and (symbolp file) (boundp file))
        (expand-file-name (symbol-value file) cm/org-directory)
      file))

  (advice-add #'org-capture-expand-file :filter-args #'cm/org-expand-variable-path)

  ;; Don't let me see the capture buffers
  (defun cm/org-bury-capture-buffer ()
    (let ((buf (buffer-base-buffer (current-buffer))))
      (bury-buffer buf)))

  (add-hook 'org-capture-before-finalize-hook #'cm/org-bury-capture-buffer)

  (use-package org-protocol
    :demand t
    :config
    ;; Built-in `org-mode' doesn't support new style protocol URI
    (define-advice org-protocol-do-capture (:override (info))
      "Perform the actual capture based on INFO."
      (let* ((temp-parts (org-protocol-parse-parameters info :new-style))
             (parts
              (cond
               ((and (listp info) (symbolp (car info))) info)
               ((= (length (car temp-parts)) 1) ;; First parameter is exactly one character long
                (org-protocol-assign-parameters temp-parts '(:template :url :title :body)))
               (t
                (org-protocol-assign-parameters temp-parts '(:url :title :body)))))
             (template (or (plist-get parts :template)
                           org-protocol-default-template-key))
             (url (and (plist-get parts :url) (org-protocol-sanitize-uri (plist-get parts :url))))
             (type (and url (if (string-match "^\\([a-z]+\\):" url)
                                (match-string 1 url))))
             (title (or (plist-get parts :title) ""))
             (region (or (plist-get parts :body) ""))
             (orglink (if url
                          (org-make-link-string
                           url (if (string-match "[^[:space:]]" title) title url))
                        title))
             (org-capture-link-is-already-stored t)) ;; avoid call to org-store-link
        (setq org-stored-links
              (cons (list url title) org-stored-links))
        (org-store-link-props :type type
                              :link url
                              :description title
                              :annotation orglink
                              :initial region
                              :query parts)
        (raise-frame)
        (funcall 'org-capture nil template)))))



(use-package ox
  :doc "Org export backends"
  :config
  (defun cm/ox-html-fix-chinese-linebreak (orig-fn paragraph contents info)
    (let* ((fix-regexp "[[:multibyte:]]")
           (fixed-contents
             (replace-regexp-in-string
              (format "\\(%s\\) *\n *\\(%s\\)" fix-regexp fix-regexp)
              "\\1\\2" contents)))
      (funcall orig-fn paragraph fixed-contents info)))

  (advice-add #'org-html-paragraph :around #'cm/ox-html-fix-chinese-linebreak))

(use-package markdown-mode
  :init
  (add-to-list 'auto-mode-alist
               `(,(rx "." (or "md" "markdown") eos) . gfm-mode)))

(provide 'init-org)
;;; init-org.el ends here
