;;; -*- lexical-binding:t ; -*-

(use-package lisp-mode
  :init
  (setq inferior-lisp-program "sbcl"))

(use-package sly
  :init
  (setq sly-command-switch-to-existing-lisp 'always)
  :config
  (defun cm/cl-cleanup-sly-maybe ()
    "Kill processes and leftover buffers when killing the last sly buffer."
    (unless (cl-loop for buf in (delq (current-buffer) (buffer-list))
               if (and (buffer-local-value 'sly-mode buf)
                       (get-buffer-window buf))
               return t)
      (dolist (conn (sly--purge-connections))
        (sly-quit-lisp-internal conn 'sly-quit-sentinel t))
      (let (kill-buffer-hook kill-buffer-query-functions)
        (mapc #'kill-buffer
              (cl-loop for buf in (delq (current-buffer) (buffer-list))
                 if (buffer-local-value 'sly-mode buf)
                 collect buf)))))

  (defun cm/cl-init-sly ()
    "Attempt to auto-start sly when opening a lisp buffer."
    (cond ((or (equal (substring (buffer-name (current-buffer)) 0 1) " ")
               (sly-connected-p)))
          ((executable-find inferior-lisp-program)
           (let ((sly-auto-start 'always))
             (sly-auto-start)
             (add-hook 'kill-buffer-hook #'cm/cl-cleanup-sly-maybe nil t)))
          ((message "WARNING: Couldn't find `inferior-lisp-program' (%s)"
                    inferior-lisp-program))))

  (add-hook 'sly-mode-hook #'cm/cl-init-sly)

  (cm/add-temp-hook #'sly-check-version
      (setq sly-protocol-version (sly-version nil (locate-library "sly.el"))))

  (sp-with-modes '(sly-mrepl-mode)
    (sp-local-pair "'" "'" :actions nil)
    (sp-local-pair "`" "`" :actions nil)))

;; (use-package sly-company
;;   :hook (sly-mode . sly-company-mode)
;;   :config
;;   (add-to-list 'company-backends 'sly-company))

(provide 'init-common-lisp)
;;; init-common-lisp.el ends here
