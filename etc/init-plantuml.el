;;; -*- lexical-binding:t ; -*-

;;* Plantuml

(use-package plantuml-mode
  :init
  (setq plantuml-jar-path (expand-file-name "plantuml.jar"
                                            cm/third-party-files-directory)
        org-plantuml-jar-path plantuml-jar-path)
  :config
  ;; Ensure the plantuml.jar installed
  (defun cm/ensure-plantuml-executable ()
    (unless (file-exists-p plantuml-jar-path)
      (url-copy-file "https://kent.dl.sourceforge.net/project/plantuml/plantuml.jar"
                     plantuml-jar-path)))

  (cm/ensure-plantuml-executable))

(provide 'init-plantuml)
;;; init-plantuml.el ends here
